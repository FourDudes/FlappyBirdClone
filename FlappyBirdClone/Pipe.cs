﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class Pipe : Sprite
    {
        public enum PipePlacement
        {
            BOTTOM,
            TOP
        };
        public PipePlacement Placement = PipePlacement.BOTTOM;

        public Pipe(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Texture2D texture) : base(graphicsDevice, spriteBatch, texture)
        {
        }

        public Pipe(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, PipePlacement placement) : base(graphicsDevice, spriteBatch)
        {
            Placement = placement;
        }

        public override void LoadResources(ContentManager content)
        {
            Texture = content.Load<Texture2D>("Textures/Pipes");

            SourceRectangle = Placement == PipePlacement.BOTTOM ? new Rectangle(Texture.Width / 2, 0, Texture.Width / 2, Texture.Height) : new Rectangle(0, 0, Texture.Width / 2, Texture.Height);
            Origin = new Vector2((float) SourceRectangle?.Width / 2, (float) SourceRectangle?.Height / 2);

            Width = (int) (SourceRectangle?.Width * ScaleX);
            Height = (int) (SourceRectangle?.Height * ScaleY);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();

            if (Texture == null)
            {
                return;
            }

            SpriteBatch.Draw(Texture, new Rectangle((int) Position.X, (int) Position.Y, Width, Height), SourceRectangle, TextureTint, Rotation, Origin, SpriteEffect, 0f);

            if (DrawCollisionBox)
            {
                PrimitiveShapeDrawer.DrawRectangleOutline(GraphicsDevice, SpriteBatch, CollisionBox.GetValueOrDefault(), 1, Color.Red);
                PrimitiveShapeDrawer.DrawRectangleOutline(GraphicsDevice, SpriteBatch, new Rectangle((int) Position.X, (int) Position.Y, 2, 2), 2, Color.Red);
            }

            SpriteBatch.End();
        }
    }
}
