﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace FlappyBirdClone
{
    public class FlappyBirdGame : Game
    {
        private readonly GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        // Static background texture
        private Texture2D backgroundTexture;

        private readonly Vector2 screenCenter;

        // Viewport dimensions
        private readonly int Width = 288;
        private readonly int Height = 510;

        // How quickly the ground and pipes move
        private readonly float scrollSpeed = 1.5f;

        // The player
        private FlappyBird player;

        // Two interchanging scrolling ground sprites
        private ScrollingGround ground1, ground2;

        // All 3 pipe sets (bottom and top) are held in this list
        private List<PipeSet> pipesList = new List<PipeSet>(3);

        // Input map holding gameplay actions and corresponding keys
        private readonly InputMapper inputMap;

        // Are pipes and ground scrolling
        private bool scrollingEnabled = true;

        // Font to draw the score
        private SpriteFont scoreFont;

        private int score;

        // Keyboard state saved in a previous update method frame (to allow for one key press checkups)
        private KeyboardState previousState;

        // Did the user start the game by pressing a button for the first time
        private bool gameStarted;

        // Value representing position change when game hasn't started yet (sinusoidal floating)
        private double floatingFactor;

        // Random number generator for pipe Y position
        private readonly Random pipeRandomizer = new Random();

        // Is player dead (collided with pipes or ground)
        private bool died;

        private bool drawCollisionBoxes = false;

        public FlappyBirdGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            IsMouseVisible = true;

            SetupGraphics();

            screenCenter = new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2);

            inputMap = new InputMapper();
            inputMap.LoadInputMap("InputMap.json");
        }

        private void SetupGraphics()
        {
            graphics.PreferredBackBufferWidth = Width;
            graphics.PreferredBackBufferHeight = Height;

            graphics.ApplyChanges();
        }

        private void StartNewGame()
        {
            pipesList.Clear();
            score = 0;
            player.Velocity = Vector2.Zero;
            player.Position = screenCenter;

            Random rand = new Random();
            for (int i = 0; i < 3; i++)
            {
                pipesList.Add(new PipeSet(graphics.GraphicsDevice, spriteBatch));
                pipesList[i].LoadResources(Content);

                int randYPosition = rand.Next(-32, 32);
                pipesList[i].BottomPipe.Position = new Vector2(Width - (pipesList[i].BottomPipe.Width / 2) + (i * 128.0f) + 256.0f, Height - (pipesList[i].BottomPipe.Height / 2) + 32.0f + randYPosition);
                pipesList[i].TopPipe.Position = new Vector2(Width - (pipesList[i].TopPipe.Width / 2) + (i * 128.0f) + 256.0f, 0 - 32.0f + randYPosition);

                pipesList[i].InitialBottomPosition = pipesList[i].BottomPipe.Position;
                pipesList[i].InitialTopPosition = pipesList[i].TopPipe.Position;
            }

            ground1.Position = new Vector2(screenCenter.X - (ground1.Texture.Width / 2), Height - ground1.Texture.Height / 2);
            ground2.Position = new Vector2(screenCenter.X + (ground2.Texture.Width / 2), Height - ground1.Texture.Height / 2);

            died = false;
            scrollingEnabled = true;
            gameStarted = false;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            backgroundTexture = Content.Load<Texture2D>("Textures/Background");

            scoreFont = Content.Load<SpriteFont>("Fonts/ScoreFont");

            player = new FlappyBird(graphics.GraphicsDevice, spriteBatch) { Position = screenCenter };
            ground1 = new ScrollingGround(GraphicsDevice, spriteBatch);
            ground2 = new ScrollingGround(GraphicsDevice, spriteBatch);

            player.LoadResources(Content);
            player.Velocity = new Vector2(0f, player.Velocity.Y);
            ground1.LoadResources(Content);
            ground2.LoadResources(Content);

            Random rand = new Random();
            for (int i = 0; i < 3; i++)
            {
                pipesList.Add(new PipeSet(graphics.GraphicsDevice, spriteBatch));
                pipesList[i].LoadResources(Content);

                int randYPosition = rand.Next(-32, 32);
                pipesList[i].BottomPipe.Position = new Vector2(Width - (pipesList[i].BottomPipe.Width / 2) + (i * 128.0f) + 256.0f, Height - (pipesList[i].BottomPipe.Height / 2) + 32.0f + randYPosition);
                pipesList[i].TopPipe.Position = new Vector2(Width - (pipesList[i].TopPipe.Width / 2) + (i * 128.0f) + 256.0f, 0 - 32.0f + randYPosition);

                pipesList[i].InitialBottomPosition = pipesList[i].BottomPipe.Position;
                pipesList[i].InitialTopPosition = pipesList[i].TopPipe.Position;
            }

            ground1.Position = new Vector2(screenCenter.X - (ground1.Texture.Width / 2), Height - ground1.Texture.Height / 2);
            ground2.Position = new Vector2(screenCenter.X + (ground2.Texture.Width / 2), Height - ground1.Texture.Height / 2);
        }


        protected override void UnloadContent()
        {
            player.UnloadResources(Content);
        }

        protected override void Update(GameTime gameTime)
        {
            if (gameTime.TotalGameTime.Milliseconds % 100 == 0)
            {
                if (floatingFactor >= Math.PI * 6)
                {
                    floatingFactor = 0;
                }

                floatingFactor += Math.PI / 6;
            }

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (previousState.IsKeyUp(inputMap["DrawCollisionBoxes"]) && Keyboard.GetState().IsKeyDown(inputMap["DrawCollisionBoxes"]))
            {
                drawCollisionBoxes = !drawCollisionBoxes;
                //player.DrawCollisionBox = !player.DrawCollisionBox;
                //ground1.DrawCollisionBox = !ground1.DrawCollisionBox;
                //ground2.DrawCollisionBox = !ground2.DrawCollisionBox;
                //foreach (var set in pipesList)
                //{
                //    set.BottomPipe.DrawCollisionBox = !set.BottomPipe.DrawCollisionBox;
                //    set.TopPipe.DrawCollisionBox = !set.TopPipe.DrawCollisionBox;
                //}
            }

            player.DrawCollisionBox = drawCollisionBoxes;
            ground1.DrawCollisionBox = drawCollisionBoxes;
            ground2.DrawCollisionBox = drawCollisionBoxes;
            foreach (var set in pipesList)
            {
                set.BottomPipe.DrawCollisionBox = drawCollisionBoxes;
                set.TopPipe.DrawCollisionBox = drawCollisionBoxes;
            }

            for (int i = 0; i < pipesList.Count; i++)
            {
                if (pipesList[i].BottomPipe.Position.X + pipesList[i].BottomPipe.Origin.X <= 0f && pipesList[i].TopPipe.Position.X + pipesList[i].TopPipe.Origin.X <= 0f)
                {
                    int randYPosition = pipeRandomizer.Next(-32, 32);
                    pipesList[i].BottomPipe.Position = pipesList[i].InitialBottomPosition + new Vector2((1 - i) * 128.0f - pipesList[i].BottomPipe.Origin.X - 256.0f, randYPosition);
                    pipesList[i].TopPipe.Position = pipesList[i].InitialTopPosition + new Vector2((1 - i) * 128.0f - pipesList[i].TopPipe.Origin.X - 256.0f, randYPosition);
                }

                if (scrollingEnabled && gameStarted)
                {
                    pipesList[i].BottomPipe.Position = new Vector2(pipesList[i].BottomPipe.Position.X - scrollSpeed,
                        pipesList[i].BottomPipe.Position.Y);
                    pipesList[i].TopPipe.Position = new Vector2(pipesList[i].TopPipe.Position.X - scrollSpeed,
                        pipesList[i].TopPipe.Position.Y);
                }

                if (player.CollisionBox.GetValueOrDefault().Intersects(pipesList[i].BottomPipe.CollisionBox.GetValueOrDefault()) ||
                    player.CollisionBox.GetValueOrDefault().Intersects(pipesList[i].TopPipe.CollisionBox.GetValueOrDefault()))
                {
                    scrollingEnabled = false;
                    player.Velocity += new Vector2(0f, 4.0f);
                    died = true;
                }

                // If player has moved past the pipe => increase score by 1
                if (!died && (player.Position.X + player.Origin.X >= pipesList[i].BottomPipe.Position.X && player.Position.X + player.Origin.X <= pipesList[i].BottomPipe.Position.X + 1f))
                {
                    score++;
                }
            }

            if (!gameStarted && !died)
            {
                player.Position = new Vector2(player.Position.X, (float) (player.Position.Y + (Math.Sin(floatingFactor))));
            }

            if (scrollingEnabled)
            {
                ground1.Position = new Vector2(ground1.Position.X - scrollSpeed, ground1.Position.Y);
                ground2.Position = new Vector2(ground2.Position.X - scrollSpeed, ground2.Position.Y);
            }

            // Swapping ground layers
            if (ground1.Position.X + (ground1.Texture.Width / 2) <= 0f)
            {
                ground1.Position = new Vector2(ground2.Position.X + ground2.Texture.Width, ground1.Position.Y);
            }

            if (ground2.Position.X + (ground2.Texture.Width / 2) <= 0f)
            {
                ground2.Position = new Vector2(ground1.Position.X + ground1.Texture.Width, ground2.Position.Y);
            }

            player.Update(gameTime);
            ground1.Update(gameTime);
            ground2.Update(gameTime);

            foreach (var set in pipesList)
            {
                set.Update(gameTime);
            }

            if (died && (previousState.IsKeyUp(inputMap["PlayerJump"]) &&
                Keyboard.GetState().IsKeyDown(inputMap["PlayerJump"]) || Mouse.GetState().LeftButton == ButtonState.Pressed))
            {
                StartNewGame();
            }

            if (!died && (previousState.IsKeyUp(inputMap["PlayerJump"]) && Keyboard.GetState().IsKeyDown(inputMap["PlayerJump"]) || Mouse.GetState().LeftButton == ButtonState.Pressed))
            {
                if (gameStarted == false)
                {
                    gameStarted = true;
                    scrollingEnabled = true;
                }

                if (player.Velocity.Y >= player.Gravity * 3)
                {
                    player.Velocity = new Vector2(player.Velocity.X, -4.0f);
                }
            }

            if (gameStarted && !died && player.Velocity.Y <= player.Gravity * 32)
            {
                player.Velocity += new Vector2(0f, player.Gravity);
            }

            if (player.Position.Y + player.Origin.Y >= (ground1.Position.Y - (ground1.Texture.Height / 2)))
            {
                player.Velocity = Vector2.Zero;
                scrollingEnabled = false;
                died = true;
                gameStarted = false;
            }

            if (gameStarted)
            {
                player.Position += player.Velocity;
            }

            previousState = Keyboard.GetState();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // Draw background
            spriteBatch.Begin();
            spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, backgroundTexture.Width, backgroundTexture.Height), null, Color.White, 0f, Vector2.Zero, SpriteEffects.None, 0f);
            spriteBatch.End();

            // Draw pipes
            foreach (var set in pipesList)
            {
                set.Draw(gameTime);
            }

            // Draw two scrolling interchanging ground layers
            ground1.Draw(gameTime);
            ground2.Draw(gameTime);

            // Draw the player (flappy)
            player.Draw(gameTime);

            // Draw the score text
            spriteBatch.Begin();
            PrimitiveShapeDrawer.DrawOutlinedText(spriteBatch, scoreFont, score.ToString(), Color.Black, Color.White, 1.0f, screenCenter - new Vector2(0f, 200.0f) - scoreFont.MeasureString(score.ToString()) / 2);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
