﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class FlappyBird : Sprite
    {
        public float Gravity = 0.3f;

        public FlappyBird(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Texture2D texture) : base(graphicsDevice, spriteBatch, texture)
        {

        }

        public FlappyBird(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) : base(graphicsDevice, spriteBatch)
        {

        }

        public override void LoadResources(ContentManager content)
        {
            Texture = content.Load<Texture2D>("Textures/FlappyBird");
        }

    }
}
