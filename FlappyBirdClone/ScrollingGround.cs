﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class ScrollingGround : Sprite
    {
        public ScrollingGround(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Texture2D texture) : base(graphicsDevice, spriteBatch, texture)
        {
        }

        public ScrollingGround(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) : base(graphicsDevice, spriteBatch)
        {
        }

        public override void LoadResources(ContentManager content)
        {
            base.LoadResources(content);
            Texture = content.Load<Texture2D>("Textures/Ground");
        }
    }
}
