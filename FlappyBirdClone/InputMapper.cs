﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.Xna.Framework.Input;
using Newtonsoft.Json;

namespace FlappyBirdClone
{
    /// <summary>
    /// Class that handles mapping keys to specific gameplay actions e.g. Key: Space -> Action: Jump.
    /// </summary>
    /// TODO: Consider making InputMapper a singleton or static class so that it can't load the same input map file many times.
    public class InputMapper
    {
        public Dictionary<string, Keys> KeyMap = new Dictionary<string, Keys>();

        /// <summary>
        /// Loads the keyboard map file and adds defined actions and their corresponding keys to the <see cref="KeyMap"/> dictionary.
        /// </summary>
        /// <param name="inputMapFile">Input map file in .json format.<para/>Example formatting:<code>{"Exit": "Escape", "Reset": "R"}</code></param>
        public void LoadInputMap(string inputMapFile)
        {
            if (!File.Exists(inputMapFile))
            {
                throw new FileNotFoundException("Input map config file not found!", inputMapFile);
            }
            
            var jsonData = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(inputMapFile));

            Console.WriteLine($"Input map file loaded: {inputMapFile}\nInput map content:");
            
            foreach (var jsonRecord in jsonData)
            {
                Console.WriteLine(jsonRecord);
                Enum.TryParse(jsonRecord.Value, true, out Keys key);
                if (KeyMap.ContainsValue(key))
                {
                    throw new JsonException($"Input map config file already has this key mapped: {key}");
                }
                KeyMap[jsonRecord.Key] = key;
            }
        }

        /// <summary>
        /// Indexer used as <see cref="KeyMap"/> dictionary accessor.
        /// </summary>
        /// <param name="actionKey">Gameplay action string e.g. "Jump"</param>
        /// <returns>Key associated with <see cref="actionKey"/> gameplay action string.</returns>
        public Keys this[string actionKey]
        {
            get { return KeyMap[actionKey]; }
            set { KeyMap[actionKey] = value; }
        }
    }
}
