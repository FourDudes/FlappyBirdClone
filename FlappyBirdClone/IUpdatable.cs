﻿using Microsoft.Xna.Framework;

namespace FlappyBirdClone
{
    public interface IUpdatable
    {
        void Update(GameTime gameTime);
    }
}
