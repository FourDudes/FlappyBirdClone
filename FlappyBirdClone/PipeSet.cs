﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class PipeSet : GameObject
    {
        public Pipe TopPipe, BottomPipe;

        public Vector2 InitialBottomPosition = Vector2.Zero;
        public Vector2 InitialTopPosition = Vector2.Zero;

        public PipeSet(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) : base(graphicsDevice, spriteBatch)
        {

        }

        public override void LoadResources(ContentManager content)
        {
            TopPipe = new Pipe(GraphicsDevice, SpriteBatch, Pipe.PipePlacement.TOP);
            BottomPipe = new Pipe(GraphicsDevice, SpriteBatch, Pipe.PipePlacement.BOTTOM);

            TopPipe.LoadResources(content);
            BottomPipe.LoadResources(content);
        }

        public override void UnloadResources(ContentManager content)
        {
            TopPipe.UnloadResources(content);
            BottomPipe.UnloadResources(content);
        }

        public override void Update(GameTime gameTime)
        {
            TopPipe.Update(gameTime);
            BottomPipe.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            TopPipe.Draw(gameTime);
            BottomPipe.Draw(gameTime);
        }
    }
}
