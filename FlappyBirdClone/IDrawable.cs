﻿using Microsoft.Xna.Framework;

namespace FlappyBirdClone
{
    public interface IDrawable
    {
        void Draw(GameTime gameTime);
    }
}
