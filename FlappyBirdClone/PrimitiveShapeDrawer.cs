﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class PrimitiveShapeDrawer
    {
        public static void DrawLine(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Texture2D pixelTexture, Vector2 a, Vector2 b, Color color, int thickness)
        {
            Color[] colorData = { color };
            pixelTexture.SetData(colorData);

            float angle = (float)Math.Atan2(b.Y - a.Y, b.X - a.X);
            float length = Vector2.Distance(a, b);

            spriteBatch.Draw(pixelTexture, a, null, color, angle, Vector2.Zero, new Vector2(length, thickness), SpriteEffects.None, 0f);
        }

        public static Texture2D GenerateCircleTexture(GraphicsDevice graphicsDevice, int radius, Color color, float sharpness)
        {
            int diameter = radius * 2;
            Texture2D circleTexture = new Texture2D(graphicsDevice, diameter, diameter, false, SurfaceFormat.Color);
            Color[] colorData = new Color[circleTexture.Width * circleTexture.Height];
            Vector2 center = new Vector2(radius);
            for (int colIndex = 0; colIndex < circleTexture.Width; colIndex++)
            {
                for (int rowIndex = 0; rowIndex < circleTexture.Height; rowIndex++)
                {
                    Vector2 position = new Vector2(colIndex, rowIndex);
                    float distance = Vector2.Distance(center, position);

                    float x = distance / diameter;
                    float edge0 = (radius * sharpness) / (float)diameter;
                    float edge1 = radius / (float)diameter;
                    float temp = MathHelper.Clamp((x - edge0) / (edge1 - edge0), 0.0f, 1.0f);
                    float result = temp * temp * (3.0f - 2.0f * temp);

                    colorData[rowIndex * circleTexture.Width + colIndex] = color * (1f - result);
                }
            }
            circleTexture.SetData(colorData);

            return circleTexture;
        }

        public static void DrawRectangleOutline(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Rectangle rectangle, int thickness, Color color)
        {
            Texture2D pixelTexture = new Texture2D(graphicsDevice, 1, 1);
            pixelTexture.SetData(new[] { color });

            // Draw top line
            spriteBatch.Draw(pixelTexture, new Rectangle(rectangle.X, rectangle.Y, rectangle.Width, thickness), color);

            // Draw left line
            spriteBatch.Draw(pixelTexture, new Rectangle(rectangle.X, rectangle.Y, thickness, rectangle.Height), color);

            // Draw right line
            spriteBatch.Draw(pixelTexture, new Rectangle((rectangle.X + rectangle.Width - thickness),
                                            rectangle.Y,
                                            thickness,
                                            rectangle.Height), color);
            // Draw bottom line
            spriteBatch.Draw(pixelTexture, new Rectangle(rectangle.X,
                                            rectangle.Y + rectangle.Height - thickness,
                                            rectangle.Width,
                                            thickness), color);
        }

        public static void DrawRectangle(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Rectangle rectangle, Color color)
        {
            Texture2D rectangleTexture = new Texture2D(graphicsDevice, rectangle.Width, rectangle.Height);
            Color[] colorData = new Color[rectangleTexture.Width * rectangleTexture.Height];
            for (int i = 0; i < colorData.Length; i++)
            {
                colorData[i] = color;
            }
            rectangleTexture.SetData(colorData);

            Vector2 position = new Vector2(rectangle.X, rectangle.Y);
            spriteBatch.Draw(rectangleTexture, position, color);
        }

        public static void DrawOutlinedText(SpriteBatch spritebatch, SpriteFont font, string text, Color backColor, Color frontColor, float scale, Vector2 position)
        {
            Vector2 origin = Vector2.Zero;

            // Outlines
            spritebatch.DrawString(font, text, position + new Vector2(1 * scale, 1 * scale), backColor, 0, origin, scale, SpriteEffects.None, 1f);
            spritebatch.DrawString(font, text, position + new Vector2(-1 * scale, 1 * scale), backColor, 0, origin, scale, SpriteEffects.None, 1f);     
            spritebatch.DrawString(font, text, position + new Vector2(-1 * scale, -1 * scale), backColor, 0, origin, scale, SpriteEffects.None, 1f);
            spritebatch.DrawString(font, text, position + new Vector2(1 * scale, -1 * scale), backColor, 0, origin, scale, SpriteEffects.None, 1f);          
                  
            // Main text
            spritebatch.DrawString(font, text, position, frontColor, 0, origin, scale, SpriteEffects.None, 0f);
        }
    }
}
