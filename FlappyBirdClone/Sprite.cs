﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace FlappyBirdClone
{
    public class Sprite : GameObject
    {
        #region GRAPHICS
        private Texture2D texture;
        public Texture2D Texture
        {
            get { return texture; }
            set
            {
                texture = value;

                // Default source rectangle to span across entire texture image (non-animated sprites)
                SourceRectangle = Texture != null ? new Rectangle(0, 0, Texture.Width, Texture.Height) : new Rectangle(0, 0, 0, 0);

                // TODO: Odd or even number check
                Origin = new Vector2((float) SourceRectangle?.Width / 2, (float) SourceRectangle?.Height / 2);

                if (Texture != null)
                {
                    Width = Texture.Width * ScaleX;
                    Height = Texture.Height * ScaleY;
                }
            }
        }

        public Rectangle? SourceRectangle { get; set; }

        public Color TextureTint { get; set; } = Color.White;

        public SpriteEffects SpriteEffect { get; set; } = SpriteEffects.None;

        public int ScaleX { get; } = 1;
        public int ScaleY { get; } = 1;

        public int Width { get; protected set; }
        public int Height { get; protected set; }
        #endregion

        #region SPACE ORIENTATION
        public float Rotation { get; set; } = 0f;

        public Vector2 Position { get; set; }
        public Vector2 Origin { get; set; }
        #endregion

        #region MOVEMENT
        public bool IsMovementEnabled { get; set; } = true;

        public Vector2 Velocity { get; set; } = Vector2.Zero;
        public Vector2 Acceleration { get; set; } = Vector2.Zero;
        #endregion

        #region STATE
        public bool IsAlive { get; set; } = true;
        #endregion

        #region COLLISION

        public Rectangle? CollisionBox { get; set; }

        #endregion

        #region DEBUG
        public bool DrawCollisionBox { get; set; }

        protected Texture2D originPointTexture;
        #endregion

        public Sprite(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch, Texture2D texture) : base(graphicsDevice, spriteBatch)
        {
            Texture = texture;

            Color[] originColorData = { Color.Red };
            originPointTexture = new Texture2D(GraphicsDevice, 1, 1);
            originPointTexture.SetData(originColorData);
        }

        public Sprite(GraphicsDevice graphicsDevice, SpriteBatch spriteBatch) : this(graphicsDevice, spriteBatch, null)
        {
        }

        public override void LoadResources(ContentManager content)
        {
        }

        public override void UnloadResources(ContentManager content)
        {
            Texture?.Dispose();
        }

        public override void Update(GameTime gameTime)
        {
            CollisionBox = new Rectangle((int) (Position.X - Origin.X), (int) (Position.Y - Origin.Y), Width, Height);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch.Begin();

            if (Texture == null)
            {
                return;
            }

            SpriteBatch.Draw(Texture, new Rectangle((int) Position.X, (int) Position.Y, Width, Height), SourceRectangle, TextureTint, Rotation, Origin, SpriteEffect, 0f);

            if (DrawCollisionBox)
            {
                PrimitiveShapeDrawer.DrawRectangleOutline(GraphicsDevice, SpriteBatch, CollisionBox.GetValueOrDefault(), 1, Color.Red);
                PrimitiveShapeDrawer.DrawRectangleOutline(GraphicsDevice, SpriteBatch, new Rectangle((int) Position.X, (int) Position.Y, 2, 2), 2, Color.Red);
            }

            SpriteBatch.End();
        }
    }
}
